
package mx.tecnm.morelia.tap_U2HILOS;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author Andrea
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        /// forma 1 de uso hilos runnable
        Thread hilo1 = new Thread(new Hilo1());
        Thread hilo2 = new Thread(new Hilo1());
        hilo1.start();
        hilo2.start();
        //// forma 2, callable no retorna valor
        ExecutorService executor = Executors.newFixedThreadPool(2);
        //alamacena el dato que tiene el procesamiento que tiene el runnable o callable
        Future <String>task1 = executor.submit(new Hilo2());
        Future <?> task2 = executor.submit(new Hilo1());
        
        while (!task1.isDone() && !task2.isDone()) {
            //tiempo muerto
        }
        System.out.println(task1.get());
        System.out.println(task1.get());
    }
    
}
